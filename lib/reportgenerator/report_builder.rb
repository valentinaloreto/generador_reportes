require_relative 'csv_builder.rb'

class ReportBuilder
  attr_accessor :queries, :choice, :response, :csv_header, :csv_body
  extend CsvBuilder

  def self.build_reports_menu
    @queries = []
    Dir.glob("sql_files/*.sql") do |file_name|
      @queries << file_name.sub!("sql_files/","").chomp!(".sql")
    end
  end

  def self.render_reports_menu
    @queries.each_with_index do |item, index|
      print "\n#{ index + 1 }. "
      print "#{ item }\n"
    end
  end

  def self.get_user_input
    loop do
      print "\nYour choice: "
      @choice = gets.chomp.to_i
      break if ( 1..@queries.size ).to_a.include?( @choice )
      print "Invalid choice. Please, try again"
    end
  end

  def self.get_sql
    query_string = File.open("sql_files/#{ @queries[@choice-1] }.sql")
    query_string.each do |line|
      @sql = line.to_s
    end
  end

  def self.get_response
    @response = ChallengeDB.run_sql(@sql)
  end

  def self.execute
    system("clear")
    build_reports_menu
    render_reports_menu
    get_user_input
    get_sql
    get_response
    build_content
    to_csv
    print "Go check your csv_files folder!\n"
  end
end
